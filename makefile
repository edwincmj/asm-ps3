empty: empty.o
	       ld -g --omagic empty.o -o empty
	              
empty.o: empty.s
	       as -g empty.s -o empty.o

clean:
	-rm -f $(wildcard *.o empty popcnt  add int3 exit exitfancy exitfancy.s)
